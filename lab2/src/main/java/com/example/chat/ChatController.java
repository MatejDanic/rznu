package com.example.chat;

import java.util.LinkedList;
import java.util.List;

import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class ChatController {
	
	private static List<String> userList;
	
	public static void init() {
		userList = new LinkedList<>();
	}

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatMessage addUser(@Payload ChatMessage chatMessage, 
                               SimpMessageHeaderAccessor headerAccessor) {
        // Add username in web socket session
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        if (userList.contains(chatMessage.getSender())) {
        	System.out.println("User " + chatMessage.getSender() + " already exists!");
        }
        
        userList.add(chatMessage.getSender());
        return chatMessage;
    }
    
    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getFoosBySimplePath() {
        return userList;
    }
}