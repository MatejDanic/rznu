from flask import Flask, jsonify, abort, make_response, request, url_for
from flask_httpauth import HTTPBasicAuth
from flask_swagger_ui import get_swaggerui_blueprint


courses = [
    {
        'id': 1,
        'title': u'Mathematics 1',
        'description': u'Real numbers and functions of one real variable. Limits of sequences. '
                       u'Limit of a real function of real variable. Derivative of a function and applications. '
                       u'Integrals and applications. Matrices, determinants and solving linear systems.',
        'semester': u'Winter',
        'lecturers': [1, 2, 3, 4]
    },
    {
        'id': 2,
        'title': u'Introduction to Programming',
        'description': u'Introduction to programming in C',
        'semester': u'Winter',
        'lecturers': [1, 3]
    },
    {
        'id': 3,
        'title': u'Digital Logic',
        'description': u'Students will gain fundamental knowledge on the structure of digital systems, '
                       u'based on levels of characteristic logic circuits and subsystems, '
                       u'as well as on applying basic methods of digital systems analysis and design, '
                       u'both combinational and sequential.',
        'semester': u'Winter',
        'lecturers': [1, 2, 4]
    },
    {
        'id': 4,
        'title': u'Physics',
        'description': u'Apply calculus techniques (derivative, integration) to analysis of physical problems',
        'semester': u'Summer',
        'lecturers': [5, 6]
    },
    {
        'id': 5,
        'title': u'Fundamentals of Electrical Engineering',
        'description': u'Students will gain fundamental knowledge on the structure of digital systems, '
                       u'based on levels of characteristic logic circuits and subsystems, '
                       u'as well as on applying basic methods of digital systems analysis and design, '
                       u'both combinational and sequential.',
        'semester': u'Summer',
        'lecturers': [1, 6, 7]
    }
]
lecturers = [
    {
        'id': 1,
        'name': u'Pero Peric',
        'department': u'Department of Applied Mathematics'
    },
    {
        'id': 2,
        'name': u'Matko Horvat',
        'department': u'Department of Applied Mathematics'
    },
    {
        'id': 3,
        'name': u'Ivan Ivanovic',
        'department': u'Department of Programming'
    },
    {
        'id': 4,
        'name': u'Jelena Pavic',
        'department': u'Department of Applied Mathematics'
    },
    {
        'id': 5,
        'name': u'Petar Pavic',
        'department': u'Department of Programming'
    },
    {
        'id': 6,
        'name': u'Marko Markovic',
        'department': u'Department of Programming'
    },
    {
        'id': 7,
        'name': u'Matej Peric',
        'department': u'Department of Programming'
    }
]

app = Flask(__name__)
auth = HTTPBasicAuth()

swagger_url = '/api'
api_url = '/static/swagger.json'
swagger_ui_blueprint = get_swaggerui_blueprint(
    swagger_url,
    api_url,
    config={
        'app_name': "Lab1"
    }
)
app.register_blueprint(swagger_ui_blueprint, url_prefix=swagger_url)


@auth.get_password
def get_password(username):
    if username == 'matej':
        return '123456'
    return None


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 403)


def make_public_course(course):
    new_course = {}
    for field in course:
        if field == 'id':
            new_course['uri'] = url_for('get_course', course_id=course['id'], _external=True)
        else:
            new_course[field] = course[field]
    return new_course


def make_public_lecturer(lecturer):
    new_lecturer = {}
    for field in lecturer:
        # print("1", field)
        if field != 'id':
            new_lecturer[field] = lecturer[field]
    for field in lecturer:
        # print("2", field)
        if field == 'id':
            new_lecturer['uri'] = url_for('get_lecturer', lecturer_id=lecturer['id'], _external=True)
        return new_lecturer


@app.route('/')
def index():
    return "http://localhost:5000/api"


# COURSES -----------------
@app.route('/api/courses', methods=['GET'])
@auth.login_required
def get_courses():
    return jsonify({'courses': [make_public_course(course) for course in courses]})


@app.route('/api/courses/<int:course_id>', methods=['GET'])
@auth.login_required
def get_course(course_id):
    course = [course for course in courses if course['id'] == course_id]
    if len(course) == 0:
        abort(404)
    return jsonify({'course': course[0]})


@app.route('/api/courses', methods=['POST'])
@auth.login_required
def create_course():
    if not request.json or 'title' not in request.json:
        abort(400)
    course = {
        'id': courses[-1]['id'] + 1,
        'title': request.json['title'],
        'semester': request.json.get('semester', ""),
        'lecturers': request.json.get('lecturers', ""),
        'description': request.json.get('description', "")
    }
    courses.append(course)
    return jsonify({'course': course}), 201


@app.route('/api/courses/<int:course_id>', methods=['PUT'])
@auth.login_required
def update_course(course_id):
    course = [course for course in courses if course['id'] == course_id]
    if len(course) == 0:
        abort(404)
    if not request.json:
        abort(400)
    if 'title' in request.json and not isinstance(request.json['title'], str):
        abort(400)
    if 'semester' in request.json and type(request.json['semester']) is not str:
        abort(400)
    if 'lecturers' in request.json and type(request.json['lecturers']) is not list:
        abort(400)
    if 'description' in request.json and type(request.json['description']) is not str:
        abort(400)
    course[0]['title'] = request.json.get('title', course[0]['title'])
    course[0]['semester'] = request.json.get('semester', course[0]['semester'])
    course[0]['lecturers'] = request.json.get('lecturers', course[0]['lecturers'])
    course[0]['description'] = request.json.get('description', course[0]['description'])
    return jsonify({'course': course[0]})


@app.route('/api/courses/<int:course_id>', methods=['DELETE'])
@auth.login_required
def delete_course(course_id):
    course = [course for course in courses if course['id'] == course_id]
    if len(course) == 0:
        abort(404)
    courses.remove(course[0])
    return jsonify({'result': True})


# LECTURERS -----------------
@app.route('/api/lecturers', methods=['GET'])
@auth.login_required
def get_lecturers():
    return jsonify({'lecturers': [make_public_lecturer(lecturer) for lecturer in lecturers]})


@app.route('/api/lecturers/<int:lecturer_id>', methods=['GET'])
@auth.login_required
def get_lecturer(lecturer_id):
    lecturer = [lecturer for lecturer in lecturers if lecturer['id'] == lecturer_id]
    if len(lecturer) == 0:
        abort(404)
    return jsonify({'lecturer': lecturer[0]})


@app.route('/api/lecturers', methods=['POST'])
@auth.login_required
def create_lecturer():
    if not request.json or 'name' not in request.json:
        abort(400)
    lecturer = {
        'id': lecturers[-1]['id'] + 1,
        'name': request.json['name'],
        'department': request.json['department']
    }
    lecturers.append(lecturer)
    return jsonify({'lecturer': lecturer}), 201


@app.route('/api/lecturers/<int:lecturer_id>', methods=['PUT'])
@auth.login_required
def update_lecturer(lecturer_id):
    lecturer = [lecturer for lecturer in lecturers if lecturer['id'] == lecturer_id]
    if len(lecturer) == 0:
        abort(404)
    if not request.json:
        abort(400)
    if 'name' in request.json and type(request.json['name']) is not str:
        abort(400)
    if 'department' in request.json and type(request.json['department']) is not str:
        abort(400)
    lecturer[0]['name'] = request.json.get('name', lecturer[0]['name'])
    lecturer[0]['department'] = request.json.get('department', lecturer[0]['department'])

    return jsonify({'lecturer': lecturer[0]})


@app.route('/api/lecturers/<int:lecturer_id>', methods=['DELETE'])
@auth.login_required
def delete_lecturer(lecturer_id):
    lecturer = [lecturer for lecturer in lecturers if lecturer['id'] == lecturer_id]
    if len(lecturer) == 0:
        abort(404)
    lecturers.remove(lecturer[0])
    return jsonify({'result': True})


# NESTED METHODS ----------------
@app.route('/api/courses/<int:course_id>/lecturers', methods=['GET'])
@auth.login_required
def get_course_lecturers(course_id):
    course = [course for course in courses if course['id'] == course_id]
    if len(course) == 0:
        abort(404)
    course_lecturers = [lecturer for lecturer in lecturers if lecturer['id'] in course[0]['lecturers']]
    return jsonify({'course lecturers': [make_public_lecturer(lecturer) for lecturer in course_lecturers]})


@app.route('/api/lecturers/<int:lecturer_id>/courses', methods=['GET'])
@auth.login_required
def get_lecturer_courses(lecturer_id):
    lecturer = [lecturer for lecturer in lecturers if lecturer['id'] == lecturer_id]
    if len(lecturer) == 0:
        abort(404)
    lecturer_courses = [course for course in courses if lecturer[0]['id'] in course['lecturers']]
    return jsonify({'lecturer courses': [make_public_course(course) for course in lecturer_courses]})


# ERROR HANDLER ----------------
@app.errorhandler(400)
def bad_request(error):
    print(error)
    return make_response(jsonify({'error': 'Bad request'}), 400)


@app.errorhandler(404)
def not_found(error):
    print(error)
    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == '__main__':
    app.run(debug=True)
